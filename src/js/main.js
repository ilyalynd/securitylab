import Swiper from 'swiper';

(() => {
  const html = document.querySelector('html');

  const mobile = window.matchMedia('(max-width: 767px)');
  const tablet = window.matchMedia('(min-width: 768px) and (max-width: 991px)');
  const laptop = window.matchMedia('(min-width: 992px) and (max-width: 1279px)');
  const desktop = window.matchMedia('(min-width: 1280px)');


  // Добавляет слайдеры "Топ за неделю" и "Лента"
  let slider,
      sliderInit = false;

  const topNewsSliderMode = () => {
    if (mobile.matches || tablet.matches) {
      if (sliderInit) return;

      sliderInit = true;
      slider = new Swiper('.slider', {
        slidesPerView: 'auto',
        spaceBetween: 16,
        breakpoints: {
          768: {
            spaceBetween: 20
          }
        }
      });
    }

    else if (laptop.matches || desktop.matches) {
      if (!sliderInit) return;

      sliderInit = false;
      slider.destroy();
    }
  }

  topNewsSliderMode();
  window.addEventListener('resize', topNewsSliderMode);


  // Открывает меню
  const linkOpenMenu = document.querySelectorAll('[data-action=open-menu]');

  const openMenu = (event) => {
    event.preventDefault();

    let menu = document.querySelector('.menu'),
        container = document.querySelector('.menu__container');

    menu.classList.add('open');
    html.style.overflowY = 'hidden';
    container.classList.add('open');

    event.stopPropagation();
  }

  linkOpenMenu.forEach(link => link.addEventListener('click', openMenu));


  // Закрывает меню
  const linkCloseMenu = document.querySelectorAll('[data-action=close-menu]');

  const closeMenu = (event) => {
    event.preventDefault();

    let menu = document.querySelector('.menu'),
        container = document.querySelector('.menu__container');

    closeLinks(event);
    closeSubmenu(event);

    container.classList.remove('open');
    setTimeout(() => {
      menu.classList.remove('open');
      html.style.overflowY = 'visible';
    }, 400);

    event.stopPropagation();
  }

  linkCloseMenu.forEach(link => link.addEventListener('click', closeMenu));


  // Открывает подменю
  const linkOpenSubmenu = document.querySelectorAll('[data-action=open-submenu]');

  const openSubmenu = (event) => {
    event.preventDefault();

    let link = event.currentTarget,
        name = link.getAttribute('data-submenu'),
        submenu = document.querySelector('[data-name=' + name + ']'),
        container = document.querySelector('.menu__container'),
        menus = document.querySelectorAll('.menu-info, .menu-social'),
        backLink = document.querySelector('.menu__link--back');

    link.parentElement.classList.add('current');
    submenu.classList.add('open');
    container.classList.add('right');
    menus.forEach(menu => menu.classList.add('hidden'));
    backLink.classList.add('open');

    event.stopPropagation();
  }

  linkOpenSubmenu.forEach(link => link.addEventListener('click', openSubmenu));


  // Открывает список ссылок подменю
  const linkOpenLinks = document.querySelectorAll('[data-action=open-links]');

  const openLinks = (event) => {
    event.preventDefault();

    let link = event.currentTarget,
        name = link.getAttribute('data-links'),
        submenu = document.querySelector('[data-name=' + name + ']'),
        container = document.querySelector('.menu__container'),
        backLink = document.querySelector('.menu__link--back');

    link.parentElement.classList.add('current');
    submenu.classList.add('open');
    container.classList.add('right-2');
    backLink.firstElementChild.setAttribute('data-action', 'close-links');

    event.stopPropagation();
  }

  linkOpenLinks.forEach(link => link.addEventListener('click', openLinks));


  // Закрывает подменю
  let linkCloseSubmenu = document.querySelector('.menu__link--back').firstElementChild;

  const closeSubmenu = (event) => {
    let links = document.querySelectorAll('.menu-navigation__item'),
        submenu = document.querySelector('.menu-navigation__submenu'),
        container = document.querySelector('.menu__container'),
        menus = document.querySelectorAll('.menu-info, .menu-social'),
        backLink = document.querySelector('.menu__link--back');

    links.forEach(link => link.classList.remove('current'));
    container.classList.remove('right');
    submenu.classList.remove('open');
    menus.forEach(menu => menu.classList.remove('hidden'));
    backLink.classList.remove('open');

    event.stopPropagation();
  }


  // Закрывает список ссылок подменю
  const closeLinks = (event) => {
    let submenu = document.querySelector('.menu-navigation__links'),
        links = document.querySelectorAll('.menu-navigation__submenu .menu-navigation__item'),
        container = document.querySelector('.menu__container'),
        backLink = document.querySelector('.menu__link--back');

    links.forEach(link => link.classList.remove('current'));
    container.classList.remove('right-2');
    submenu.classList.remove('open');
    backLink.firstElementChild.setAttribute('data-action', 'close-submenu');

    event.stopPropagation();
  }

  linkCloseSubmenu.addEventListener('click', (event) => {
    event.preventDefault();

    let action = linkCloseSubmenu.getAttribute('data-action');

    if (action == 'close-submenu') {
      closeSubmenu(event);
    } else if (action == 'close-links') {
      closeLinks(event);
    }
  });


  // Обновляет логотип
  const asciiArt = document.querySelector('.header-ascii__art');

  const updateLogo = () => {
    let numbers = Array.from(Array(9), (_, index) => index + 1),
        randomIndex = Math.floor(Math.random() * numbers.length),
        image = document.createElement('img');

    image.setAttribute('src', '/image/ascii/logo-' + numbers[randomIndex] + '.svg');
    image.setAttribute('class', 'logo-' + numbers[randomIndex]);
    image.setAttribute('alt', 'SecurityLab');

    return image;
  }

  asciiArt.appendChild(updateLogo());


  // Анимирует заголовки
  const animationTitle = document.querySelectorAll('.animation-decode');

  const decodeText = (title) => {
    // Разбиваем строку на слова
    let words = title.textContent.replace('\xa0', ' ').split(' ');

    title.innerHTML = '';

    // Создаём новую структуру заголовка
    words.forEach((word, index, array) => {
      // Разбиваем слова на символы
      let wordChars = word.split(''),
          wordSpan = document.createElement('span');

      for (let i = 0; i < wordChars.length; i++) {
        let span = document.createElement('span');

        span.classList.add('animation-char');
        span.textContent = wordChars[i];

        wordSpan.appendChild(span);
      };

      wordSpan.classList.add('animation-word');

      title.appendChild(wordSpan);

      if (index !== array.length - 1) {
        let span = document.createElement('span');

        span.classList.add('space');
        span.textContent = ' ';

        title.appendChild(span);
      }
    });

    let text = document.getElementsByClassName('animation-decode')[0];
    let childs = text.querySelectorAll('.animation-char, .space');

    // Assign the placeholder array its places
    let state = [], classes;

    for (let i = 0; i < childs.length; i++) {
      childs.forEach(child => {
        child.classList.remove('state-1', 'state-2', 'state-3');
        state[i] = i;
      });
    }

    // Shuffle the array to get new sequences each time
    let shuffled = shuffle(state);

    for (let i = 0; i < shuffled.length; i++) {
      childs.forEach(() => {
        let child = childs[shuffled[i]];
        classes = child.classList;

        // Fire the first one at random times
        let state1Time = Math.round(Math.random() * (20000 - 300)) + 50;

        if (classes.contains('animation-char')) {
          setTimeout(firstStages.bind(null, child), state1Time);
        }
      });
    }
  }

  // Send the node for later .state changes
  function firstStages(child) {
    if (child.classList.contains('state-2')) {
      child.classList.add('state-3');
    }

    else if (child.classList.contains('state-1')) {
      child.classList.add('state-2');
    }

    else if (!child.classList.contains('state-1')) {
      child.classList.add('state-1');
      setTimeout(secondStages.bind(null, child), 100);
    }
  }

  function secondStages(child) {
    if (child.classList.contains('state-1')) {
      child.classList.add('state-2');
      setTimeout(thirdStages.bind(null, child), 100);
    }

    else if (!child.classList.contains('state-1')) {
      child.classList.add('state-1');
    }
  }

  function thirdStages(child) {
    if (child.classList.contains('state-2')) {
      child.classList.add('state-3');
    }
  }

  function shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  // animationTitle?.forEach(title => {
  //   decodeText(title);
  //   window.addEventListener('resize', () => {
  //     decodeText(title);
  //   });
  // });
})();
