# SecurityLab change log

## Version 1.0.0 under development

- New: Added templates for all pages
- New: Added fonts
- New: Added Swiper
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init June 28, 2023
